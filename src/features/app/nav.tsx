import React from "react";
import NavItem from "components/nav/NavLeft/NavItem";


export default function AppNav() {
    return (
        <NavItem to={'/'} primary={'Home'} exact />
    );
}

