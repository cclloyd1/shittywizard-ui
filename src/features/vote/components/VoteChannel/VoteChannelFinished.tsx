import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Divider, IconButton, Tooltip} from "@material-ui/core";
import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import AddVote from "../AddVote";
import {useVote} from "../VoteProvider";


const useStyles = makeStyles((theme: any) => ({
    root: {
        padding: theme.spacing(2),
        boxShadow: 'inset 0 0 20px rgba(0,200,0,.25)',
    },
    choices: {
        margin: 0,
        paddingLeft: theme.spacing(3),
        fontSize: 16,
    },
    dates: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    add: {
        fontSize: 16,
        display: 'flex',
        alignItems: 'center',
    },
    right: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
    }
}));

export default function VoteChannelFinished() {
    const classes = useStyles();

    const { channel, vote, addVote } = useVote();

    const [addActive, setAddActive] = useState(false);

    const toggleAddActive = () => {
        setAddActive(!addActive);
    }


    return (
        <Paper className={classes.root}>
            <Grid container spacing={1}>
                <Grid item xs={9}>
                    <Typography variant={'subtitle2'}>#{channel.name}</Typography>
                    <Typography variant={'h5'}>{vote.title}</Typography>
                </Grid>
                <Grid item xs={3} className={classes.right}>
                    <Tooltip title={'Start new vote'}>
                        <IconButton onClick={toggleAddActive}><AddCircleIcon/></IconButton>
                    </Tooltip>
                </Grid>
                <Grid item xs={12}>
                    <Divider/>
                </Grid>
                <Grid item xs={12}>
                    <AddVote open={addActive} toggleOpen={toggleAddActive} />
                </Grid>
                <Grid item xs={12}><Typography variant={'body1'} className={classes.dates}>
                        <span>
                            <b>WINNER{vote.winner.length > 1 && 'S'}: </b>
                            {vote.winner.map((winner: number, i: number) => {
                                if (i >= vote.winner.length-1)
                                    return <span key={i}>{vote.choices[winner]}</span>
                                return <span key={i}>{vote.choices[winner]}, </span>
                            })}
                        </span>
                    <span><b>VOTE FINISHED</b></span>
                </Typography></Grid>
                <Grid item xs={12}>
                    <ol className={classes.choices}>
                        {vote.choices.map((c: string) => <li key={c}>{c}</li>)}
                    </ol>
                </Grid>
            </Grid>
        </Paper>
    )
}