import React, {useContext, useEffect, useState} from 'react';
import {Vote, VOTE_STATUS} from "../types";
import {api} from "../../auth/authSlice";
import {connect} from "react-redux";
import {useToasts} from "react-toast-notifications";


const VoteContext = React.createContext<any>({});


function VoteProvider(props: any) {
    const { children, api } = props;
    const { addToast } = useToasts();

    const [vote, setVote] = useState<Vote|null>(props.vote);
    const [channel, setChannel] = useState<any>(props.channel);
    const [status, setStatus] = useState<number>(4);
    const [editActive, setEditActive] =  useState<boolean>(false);

    useEffect(() => {
        if (vote) {
            if (vote.started) {
                if (vote.finished) {
                    if (vote.winner.length > 0) {
                        setStatus(VOTE_STATUS.FINISHED);
                        return
                    }
                    else {
                        setStatus(VOTE_STATUS.TERMINATED);
                        return
                    }
                }
                else {
                    setStatus(VOTE_STATUS.ACTIVE);
                    return
                }
            }
            else {
                if (vote.finished) {
                    setStatus(VOTE_STATUS.TERMINATED);
                    return;
                }
                else {
                    setStatus(VOTE_STATUS.PREVOTE);
                    return
                }
            }
        }
        else {
            setStatus(VOTE_STATUS.NOVOTE);
            return;
        }
    }, [vote]);

    const toggleEditActive = () => {
        setEditActive(!editActive);
    }

    const updateVote = (data: any) => {
        api(`/api/vote/${data.id}`, {
            method: 'PUT',
            data: {
                ...data,
            }
        }).then((data: any) => {
            console.log('Update vote data', data);
            setVote(data);
            return true;
        }).catch((err: any) => {
            console.error('Error updating vote', err);
            addToast(err.detail, {appearance: 'error'});
            return false;
        });
    }

    const addVote = (data: any) => {
        api('/api/vote/', {
            method: 'POST',
            data: {
                ...data,
            }
        }).then((data: any) => {
            console.log('New vote data', data);
            setVote(data);
        }).catch((err: any) => {
            console.error('Error adding vote', err);
            addToast(err.detail, {appearance: 'error'})
        });
    }

    const endVote = (voteID: number) => {
        api(`/api/vote/${voteID}/end/`, {
            method: 'GET',
        }).then((data: any) => {
            console.log('Terminated vote', data);
            setVote(data);
        }).catch((err: any) => {
            console.error('Error ending vote', err);
            addToast(err.detail, {appearance: 'error'})
        });
    }

    const startVote = (voteID: number) => {
        api(`/api/vote/${voteID}/start/`, {
            method: 'GET',
        }).then((data: any) => {
            console.log('Started vote', data);
            setVote(data);
        }).catch((err: any) => {
            console.error('Error starting vote', err);
            addToast(err.detail, {appearance: 'error'})
        });
    }

    const declareWinner = (voteID: number) => {
        api(`/api/vote/${voteID}/winner/`, {
            method: 'GET',
        }).then((data: any) => {
            console.log('Got winner from vote', data);
            setVote(data);
        }).catch((err: any) => {
            console.error('Error declaring winner of vote', err);
            addToast(err.detail, {appearance: 'error'})
        });
    }

    return (
        <VoteContext.Provider value={{
            channel,
            vote,
            status,
            addVote,
            startVote,
            declareWinner,
            endVote,
            setVote,
            updateVote,
            editActive,
            toggleEditActive,
        }}>
            {children}
        </VoteContext.Provider>
    );
}


export const useVote = () => {
    const ctx = useContext(VoteContext);

    if (!ctx) {
        throw Error('The `useVote` hook must be called from a descendent of the `VoteProvider`.');
    }

    return {
        channel: ctx.channel,
        vote: ctx.vote,
        voteStatus: ctx.status,
        addVote: ctx.addVote,
        startVote: ctx.startVote,
        declareWinner: ctx.declareWinner,
        endVote: ctx.endVote,
        setVote: ctx.setVote,
        updateVote: ctx.updateVote,
        editActive: ctx.editActive,
        toggleEdit: ctx.toggleEditActive,
    };
};

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    api,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VoteProvider)