import Grid from "@material-ui/core/Grid";
import {Button, Collapse, TextField} from "@material-ui/core";
import {DateTimePicker} from "@material-ui/pickers";
import add from "date-fns/add";
import React, {useState} from "react";
import sub from "date-fns/sub";
import endOfWeek from "date-fns/endOfWeek";
import compareAsc from "date-fns/compareAsc";
import formatISO from "date-fns/formatISO";
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from "react-hook-form";
import {useVote} from "./VoteProvider";


const useStyles = makeStyles((theme: any) => ({
    root: {
        marginTop: theme.spacing(1),
    },
}));


export default function AddVote(props: any) {
    const classes = useStyles();
    const { register, handleSubmit } = useForm();
    const { channel, addVote } = useVote();
    const { open } = props;

    // Default date is Friday@Noon, and Saturday@Noon
    const [start, setStart] = useState<Date>(sub(endOfWeek(new Date(), {weekStartsOn: 6}), {hours: 11, minutes: 59}));
    const [end, setEnd] = useState<Date>(add(endOfWeek(new Date(), {weekStartsOn: 6}), {hours: 12, minutes: 1}));

    const handleStartChange = (value: any) => {
        setStart(value);
        if (compareAsc(value, end) >= 0) {
            setEnd(add(value, {days: 1}));
        }
    }

    const handleEndChange = (value: any) => {
        setEnd(value);
    }

    const onSubmit = (data: any) => {
        addVote({
            title: data.title,
            start: formatISO(start),
            end: formatISO(end),
            channel: channel.id,
        })
    }


    return (
        <Collapse in={open}>
            <form id={`add-vote-${channel.id}`} onSubmit={handleSubmit(onSubmit)}>
                <Grid container spacing={1} className={classes.root}>
                    <Grid item xs={9}>
                        <TextField
                            fullWidth
                            variant={'outlined'}
                            placeholder={'New vote title'}
                            size={'small'}
                            label={'Question Title'}
                            name={'title'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <Button fullWidth variant={'contained'} color={'primary'} type={'submit'}>Add</Button>
                    </Grid>
                    <Grid item xs={6}>
                        <DateTimePicker
                            value={start}
                            onChange={handleStartChange}
                            disablePast
                            label={'Start'}
                            name={'start'}
                            size={'small'}
                            showTodayButton
                            inputVariant={'outlined'}
                            minutesStep={5}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <DateTimePicker
                            value={end}
                            onChange={handleEndChange}
                            disablePast
                            label={'End'}
                            name={'end'}
                            size={'small'}
                            showTodayButton
                            inputVariant={'outlined'}
                            minutesStep={5}
                            minDate={add(start, {minutes: 5})}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
            </form>
        </Collapse>
    );
}
