import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import {Divider} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import VoteChannel from "./VoteChannel";
import VoteProvider from "./VoteProvider";


const useStyles = makeStyles((theme: any) => ({
    root: {
        marginBottom: theme.spacing(3),
    },
    divider: {
        marginBottom: theme.spacing(1),
    }
}));


export function timeDifference(date1: string, date2: string) {
    // @ts-ignore
    const diff = new Date(date1) - new Date(date2);
    const s = diff / 1000;
    return `${s/86400}d`
}


function VoteCategory(props: any) {
    const classes = useStyles();

    const { category, channels } = props.category;

    return (
        <Box className={classes.root}>
            <Typography variant={'h4'}>{category.name}</Typography>
            <Divider className={classes.divider}/>
            <Grid container spacing={1}>
                {channels.map((c: any) =>
                    <Grid item xs={12} md={6} key={c.channel.id}>
                        <VoteProvider channel={c.channel} vote={c.vote}>
                            <VoteChannel />
                        </VoteProvider>
                    </Grid>
                )}
            </Grid>
        </Box>
    );
}

export default VoteCategory;

