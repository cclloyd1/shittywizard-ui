import * as React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import PropTypes from 'prop-types';
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import SoundbiteCategoryHeader from "./SoundbiteCategoryHeader";
import Box from "@material-ui/core/Box";
import SoundbiteButton from "./SoundbiteButton";
import {Soundbite} from 'features/soundbites/types';

const useStyles = makeStyles(theme => ({
    text: {
        marginTop: theme.spacing(3),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    soundbites: {

    }
}));

export default function SoundbiteCollection(props: any) {
    const classes = useStyles();
    const { title, soundbites, id } = props;

    return (
        <>
            <SoundbiteCategoryHeader title={title}/>

            <Box className={classes.soundbites}>
                {soundbites.map((n: Soundbite) =>
                    <SoundbiteButton transcript={n.transcript} id={n.id} key={n.id} name={n.name}/>
                )}
            </Box>
        </>
    );
}

SoundbiteCollection.defaultProps = {
    soundbites: [],
    title: '',
    id: 0,
};
