import ProtectedRoute from "../../components/ProtectedRoute";
import React from "react";
import PageVoteAll from "./pages/PageVoteAll";
import PageVoteNew from "./pages/PageVoteNew";
import PageVoteSingle from "./pages/PageVoteSingle";

const VoteRoutes = (
    <>
        <ProtectedRoute path='/votes/all' exact><PageVoteAll/></ProtectedRoute>
        <ProtectedRoute path='/votes/new' exact><PageVoteNew/></ProtectedRoute>
        <ProtectedRoute path='/votes/:id' exact><PageVoteSingle/></ProtectedRoute>
    </>
)

export default VoteRoutes;