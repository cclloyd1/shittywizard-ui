import React, {useEffect} from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {Box, Grid, TextField} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import {login, logout} from "./authSlice";
import {connect} from "react-redux";
import {useForm} from "react-hook-form";
import {makeStyles} from "@material-ui/core/styles";


type AccountFormData = {
    username: string,
    password: string,
}


const useStyles = makeStyles(theme => ({
    loginBox: {
        padding: theme.spacing(0, 1),
    },
    divider: {
        margin: theme.spacing(1, 0,)
    },
    ssoLoginIcon: {
        //height: theme.spacing(5),
        //width: 'auto',
        display: 'inline-block',
    },
}));


function AuthNav(props: any) {
    const classes = useStyles();
    const { register, handleSubmit } = useForm();

    const { isAuthenticated } = props;
    const { login, logout } = props;

    const [logoutOpen, setLogoutOpen] = React.useState<boolean>(false);
    const [clientID, setClientID] = React.useState<string>('');

    const [oAuthURL, setOAuthURL] = React.useState<string>();

    useEffect(() => {
        fetch('/social/discord/oauth/client/').then(r => {
            if (!r.ok)
                throw r;
            else
                return r.json();
        }).then(data => {
            setClientID(data);
            let port = ''
            if (window.location.port != '80' && window.location.port != '443')
                port = `:${window.location.port}`
            const hostname = encodeURIComponent(`${window.location.protocol}//${window.location.hostname}${port}/authorize`)
            setOAuthURL(`https://discord.com/api/oauth2/authorize?client_id=${clientID}&redirect_uri=${hostname}&response_type=code&scope=identify%20email`);
        }).catch(err => {

        })
    }, []);


    const handleLogout = () => {
        logout();
        toggleDialog();
    }

    const toggleDialog = () => {
        setLogoutOpen(!logoutOpen);
    }

    const handleLogin = (data: AccountFormData) => {
        login(data.username, data.password);
    }

    if (isAuthenticated) {
        return (
            <>
                <ListItem button onClick={toggleDialog}><ListItemText primary={'Logout'}/></ListItem>
                <Dialog open={logoutOpen} onClose={toggleDialog}>
                    <DialogTitle>{"Confirm"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Are you sure you want to log out?</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleLogout} color="primary">Logout</Button>
                        <Button onClick={toggleDialog} color="primary" autoFocus>Cancel</Button>
                    </DialogActions>
                </Dialog>
            </>
        )
    }
    else {
        return (
            <>
                <Box className={classes.loginBox}>
                    <form id={'nav-login'} onSubmit={handleSubmit(handleLogin)}>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    placeholder={'Username'}
                                    name={'username'}
                                    variant={'outlined'}
                                    label={'Username'}
                                    inputRef={register}
                                    autoComplete={'off'}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    placeholder={'Password'}
                                    name={'password'}
                                    variant={'outlined'}
                                    label={'Password'}
                                    type={'password'}
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button variant={'contained'} fullWidth color={'primary'} type={'submit'}>Login</Button>
                            </Grid>
                            <Grid item xs={12}>
                                <Button variant={'contained'} fullWidth color={'primary'} type={'submit'}>
                                    <a href={oAuthURL} target={'_SELF'}>
                                        <img src={'https://discord.com/assets/192cb9459cbc0f9e73e2591b700f1857.svg'} className={classes.ssoLoginIcon} alt={'login'}/>
                                    </a>
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </Box>
                <Divider className={classes.divider}/>
            </>
        )
    }
}

const mapStateToProps = (state: any) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = {
    login,
    logout,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthNav)

