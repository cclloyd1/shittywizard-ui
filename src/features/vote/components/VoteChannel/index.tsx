import React from "react";
import VoteChannelPreVote from "./VoteChannelPreVote";
import VoteChannelActive from "./VoteChannelActive";
import VoteChannelFinished from "./VoteChannelFinished";
import VoteChannelTerminated from "./VoteChannelTerminated";
import VoteChannelNoVote from "./VoteChannelNoVote";
import {useVote} from "../VoteProvider";
import {VOTE_STATUS} from "../../types";


export default function VoteChannel() {
    const { voteStatus } = useVote();

    return (
        <>
            {voteStatus === VOTE_STATUS.NOVOTE && <VoteChannelNoVote/>}
            {voteStatus === VOTE_STATUS.PREVOTE && <VoteChannelPreVote/>}
            {voteStatus === VOTE_STATUS.ACTIVE && <VoteChannelActive/>}
            {voteStatus === VOTE_STATUS.FINISHED && <VoteChannelFinished/>}
            {voteStatus === VOTE_STATUS.TERMINATED && <VoteChannelTerminated/>}
        </>
    )

}
