import React from "react";
import NavItem from "components/nav/NavLeft/NavItem";


export default function SoundbiteNav() {
    return (
        <NavItem to={'/soundbites'} primary={'Soundbites'} />
    );
}

