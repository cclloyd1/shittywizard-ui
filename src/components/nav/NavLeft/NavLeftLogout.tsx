import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "@material-ui/core/Button";
import {connect} from "react-redux";
import {logout} from "../../../features/auth/authSlice";


function NavLeftLogout(props: any) {

    // Redux callbacks
    const { logout } = props;

    // Local state
    const [logoutOpen, setLogoutOpen] = React.useState<boolean>(false);


    const handleLogout = () => {
        logout();
        toggleDialog();
    }

    const toggleDialog = () => {
        setLogoutOpen(!logoutOpen);
    }


    return (
        <>
            <ListItem button onClick={toggleDialog}><ListItemText primary={'Logout'}/></ListItem>
            <Dialog
                open={logoutOpen}
                onClose={toggleDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Confirm"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure you want to log out?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleLogout} color="primary">
                        Logout
                    </Button>
                    <Button onClick={toggleDialog} color="primary" autoFocus>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    logout,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(NavLeftLogout)