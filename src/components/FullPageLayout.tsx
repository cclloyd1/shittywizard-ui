import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {appBarHeight, discordColors} from "../app/theme";
import PageHeader from "./PageHeader";
import {Box, Paper} from "@material-ui/core";
import clsx from "clsx";
import {appName} from "../app/App";


const useStyles = makeStyles(theme => ({
    root: (props: any) => ({
        minHeight: `calc(100vh - ${appBarHeight}px - ${theme.spacing(1)}px - 1px)`,
        //minHeight: '100%',
        display: 'flex',
        paddingLeft: 0,
        paddingRight: 0,
    }),
    middle: {
        alignItems: 'center',
    },
    box: (props: any) => ({
        marginBottom: theme.spacing(1),
        padding: theme.spacing(props.padding),
        minWidth: '100%',
    }),
    paper: (props: any) => ({
        backgroundColor: discordColors.dark,
        marginBottom: theme.spacing(1),
        padding: theme.spacing(props.padding),
        minWidth: '100%',
    }),
}));

export default function FullPageLayout(props: any) {
    const {children, maxWidth, PageTitleProps, PaperProps, paper, middle, padding, mobilePadding, header, title, className} = props;
    const pagePadding = window.innerWidth >= 960 ? padding : mobilePadding;
    const classes = useStyles({
        padding: pagePadding,
    } as any);


    let pageTitle = '';
    let pageHeader = '';
    if (title !== '' && header === '') {
        pageTitle = title;
        pageHeader = title;
    } else if (title === '' && header !== '') {
        pageTitle = header;
        pageHeader = header;
    }
    else {
        pageTitle = title;
        pageHeader = header;
    }


    useEffect(() => {
        document.title = `${appName} · ${pageTitle}`;
    }, []);

    const content =
        <>
            {props.header !== '' && <PageHeader title={pageHeader} {...PageTitleProps }/>}
            {children}
        </>


    if (paper)
        return (
            <Container maxWidth={maxWidth} fixed className={clsx(classes.root, {
                [classes.middle]: middle,
            })}>
                <Paper className={classes.paper} {...PaperProps} >
                    {content}
                </Paper>
            </Container>
        )
    return (
        <Container maxWidth={maxWidth} fixed className={clsx(classes.root, {
            [classes.middle]: middle,
        })}>
            <Box className={classes.box}>
                {content}
            </Box>
        </Container>
    )

}

FullPageLayout.defaultProps = {
    maxWidth: 'md' as string,
    PageTitleProps: {} as any,
    PaperProps: {} as any,
    header: '' as string,
    paper: false as boolean,
    middle: false as boolean,
    padding: 2 as number,
    mobilePadding: 2 as number,
}

