import PageLogin from "./pages/PageLogin";
import {Route} from "react-router-dom";
import React from "react";
import PageAuthorize from "./pages/PageAuthorize";


const AuthRoutes = (
    <>
        <Route path='/login' exact key={'login'}><PageLogin/></Route>
        <Route path='/authorize' exact key={'authorize'}><PageAuthorize/></Route>
    </>
)

export default AuthRoutes;
