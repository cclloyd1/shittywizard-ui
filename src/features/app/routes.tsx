import PageHome from "./pages/PageHome";
import React from "react";
import ProtectedRoute from "components/ProtectedRoute";


const AppRoutes = (
    <>
        <ProtectedRoute path='/' exact><PageHome/></ProtectedRoute>
    </>
)

export default AppRoutes;
