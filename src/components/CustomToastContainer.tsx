import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
// @ts-ignore
import {DefaultToastContainer} from 'react-toast-notifications';


const useStyles = makeStyles(theme => ({
    root: {
        zIndex: '5000 !important' as any,
    }
}));

export default function CustomToastContainer(props: any) {
    const classes = useStyles();
    const { children, appearance } = props;

    return (
        <DefaultToastContainer {...props} className={classes.root} >
            {children}
        </DefaultToastContainer>
    );
}

