import React from "react";
import NavItem from "components/nav/NavLeft/NavItem";


export default function VoteNav() {
    /*
    return (
        <NavItem collapsible primary={'Voting'} path={'/votes'}>
            <NavItem to={'/votes/all'} primary={'All Channels'} />
            <NavItem to={'/votes/new'} primary={'New Vote'} />
        </NavItem>
    );*/
    return (
        <NavItem to={'/votes/all'} primary={'Votes'} />
    );
}

