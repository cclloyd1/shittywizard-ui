import React from 'react';
import 'assets/App.css';
import CssBaseline from "@material-ui/core/CssBaseline";
import {ToastProvider} from "react-toast-notifications";
import {ThemeProvider} from '@material-ui/core/styles';
import MUIToastContainer from "components/MUIToastContainer";
import MUIToast from "components/MUIToast";
import theme from "app/theme";
import {BrowserRouter as Router} from 'react-router-dom';
import 'typeface-roboto';
import {Provider} from "react-redux";
import {store} from "app/store";
import MaterialUIApp from "./MaterialUIApp";
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider} from "@material-ui/pickers";


export const urlPattern = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/
export const appName = 'ShittyWizard';


function App() {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <ToastProvider autoDismissTimeout={3000} autoDismiss={true} placement="top-center" components={{ ToastContainer: MUIToastContainer, Toast: MUIToast }}>
                    <Router>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <CssBaseline />
                            <MaterialUIApp />
                        </MuiPickersUtilsProvider>
                    </Router>
                </ToastProvider>
            </ThemeProvider>
        </Provider>
    );
}

export default App;
