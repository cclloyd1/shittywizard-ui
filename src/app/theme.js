import {blue} from "@material-ui/core/colors";
import {PaletteOptions} from "@material-ui/core/styles/createPalette";
//import {createMuiTheme} from '@material-ui/core/styles';
// TODO: Remove in v5 of material-ui
import {darken, unstable_createMuiStrictModeTheme as createMuiTheme} from '@material-ui/core';


export const drawerWidth = 200;
export const appBarHeight = 55;
/*
export const discordColors = {
    blurple: '#7289DA',
    white: '#FFFFFF',
    black: '#000000',
    greple: '#99AAB5',
    dark: '#2C2F33',
    notblack: '#23272A',
    grey: {
        100: '#40444b',
        200: '#36393f',
        300: '#32353b',
        400: '#2f3136',
        500: '#292b2f',
        600: '#202225',
        'text': '#dcddde',
    }
};*/

export const discordColors = {
    blurple: darken('#7289DA', 0.25),
    white: '#ffffff',
    black: '#000000',
    greple: '#99AAB5',
    dark: '#141414',
    notblack: '#111111',
    grey: {
        100: '#242424',
        200: '#141414',
        300: '#111111',
        400: '#0C0C0C',
        500: '#141414',
        600: '#141414',
        'text': '#dcddde',
    },
    greyold: {
        100: '#40444b',
        200: '#36393f',
        300: '#32353b',
        400: '#2f3136',
        500: '#292b2f',
        600: '#202225',
        'text': '#dcddde',
    }
}


// Create a theme instance.
const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: discordColors.blurple,
            contrastThreshold: 3,
        },
        secondary: {
            main: blue[800],
        },
        text: {
            primary: '#b3b3b3',
        },
        dark: {
            1: '#202225',
            2: '#2b2c31',
            3: '#2f3136',
            4: '#36393e',
            5: '#42464d',
            6: '#484b52',
        }
    },
    appBar: {
        height: appBarHeight,
    },
});


export default theme;