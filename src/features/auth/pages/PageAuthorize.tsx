import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import { Redirect }from "react-router-dom";
import queryString from 'query-string';
import FullPageLayout from "components/FullPageLayout";
import {loginWithDiscord} from "features/auth/authSlice";
import {connect} from "react-redux";
import {useToasts} from "react-toast-notifications";
import Box from "@material-ui/core/Box";
import {discordColors} from "app/theme";
import {Typography} from "@material-ui/core";
import DiscordSpinner from "components/DiscordSpinner";


const useStyles = makeStyles(theme => ({
    loadingBox: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        padding: theme.spacing(3),
    },
    loadingIcon: {
        color: discordColors.blurple,
    },
}));


function PageAuthorize(props: any) {
    const classes = useStyles();

    const { addToast } = useToasts();

    const { isAuthenticated } = props;
    const { loginWithDiscord } = props;

    const [authenticating, setAuthenticating] = React.useState<boolean>(true);

    const params = queryString.parse(window.location.search);

    const handleLogin = () => {
        if (!isAuthenticated && params.code) {
            setAuthenticating(true);
            console.log('code', params.code, params);
            loginWithDiscord(params.code).catch((err: any) => {
                addToast(err.msg, {appearance: 'error'})
            })
        }
    }

    useEffect(handleLogin, []);


    return (
        <FullPageLayout title={'Authorize'}>
            {isAuthenticated && <Redirect to={'/'}/>}
            <Box className={classes.loadingBox}>
                <Typography variant={'h4'} gutterBottom>Authorizing</Typography>
                <DiscordSpinner/>
            </Box>
        </FullPageLayout>
    );

}

const mapStateToProps = (state: any) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = {
    loginWithDiscord,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageAuthorize)
