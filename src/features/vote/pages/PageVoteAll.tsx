import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {discordColors} from "app/theme";
import Box from "@material-ui/core/Box";
import {useToasts} from "react-toast-notifications";
import FullPageLayout from "components/FullPageLayout";
import {connect} from "react-redux";
import {api} from "features/auth/authSlice";
import DiscordSpinner from "components/DiscordSpinner";
import VoteCategory from "../components/VoteCategory";


const useStyles = makeStyles((theme: any) => ({
    loadingBox: {
        display: 'flex',
        justifyContent: 'center',
        padding: theme.spacing(3),
    },
    loadingIcon: {
        color: discordColors.blurple,
    },
}));


function PageVoteAll(props: any) {
    const classes = useStyles();

    const { addToast } = useToasts();

    const { api } = props;

    const [categories, setCategories] = React.useState<any>([]);
    const [categoriesLoading, setCategoriesLoading] = React.useState<boolean>(true);

    const fetchAllVotes = () => {
        api('/api/vote/all/').then((data: any) => {
            setCategories([...data]);
            setCategoriesLoading(false);
        }).catch((err: any) => {
            console.error('Error fetching votes', err);
            addToast(err.detail, {appearance: 'error'});
        });
    }

    useEffect(fetchAllVotes, []);

    return (
        <FullPageLayout title={'Votes'} header={'Votes in all channels'}>
            {!categoriesLoading && categories.map((c: any) => <VoteCategory category={c} key={c.category.id}/> )}
            {categoriesLoading  && <Box className={classes.loadingBox}><DiscordSpinner size={50} /></Box>}
        </FullPageLayout>

    );

}

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = {
    api,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageVoteAll)