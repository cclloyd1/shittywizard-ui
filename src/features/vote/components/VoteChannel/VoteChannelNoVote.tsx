import Typography from "@material-ui/core/Typography";
import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import {Divider, Grid, IconButton, Tooltip} from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import AddVote from "../AddVote";
import clsx from "clsx";
import {useVote} from "../VoteProvider";
import EditIcon from "@material-ui/icons/Edit";


const useStyles = makeStyles((theme: any) => ({
    root: {
        padding: theme.spacing(2),
        opacity: 0.35,
        transitionProperty: 'opacity',
        transitionDuration: theme.transitions.duration.short,
        '&:hover': {
            opacity: 1,
        }
    },
    end: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    active: {
        opacity: 1,
    },
    buttons: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
}));

export default function VoteChannelNoVote(props: any) {
    const classes = useStyles();

    const { channel } = useVote();

    const [addActive, setAddActive] = useState(false);

    const toggleAddActive = () => {
        setAddActive(!addActive);
    }

    return (
        <Paper className={clsx(classes.root, {
            [classes.active]: addActive,
        })}>
            <Grid container spacing={1}>
                <Grid item xs={9}>
                    <Typography variant={'subtitle2'}>#{channel.name}</Typography>
                    <Typography variant={'body2'}>No vote in this channel</Typography>
                </Grid>
                <Grid item xs={3} className={classes.end}>
                    <Tooltip title={'Start new vote'}>
                        <IconButton onClick={toggleAddActive}><AddCircleIcon/></IconButton>
                    </Tooltip>
                </Grid>


                <Grid item xs={12}>
                    <AddVote open={addActive} toggleOpen={toggleAddActive} />
                </Grid>

            </Grid>
        </Paper>
    )
}